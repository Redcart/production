from flask import Flask, request, jsonify
import tensorflow as tf
from tensorflow.keras.models import model_from_json
from tensorflow.keras.losses import SparseCategoricalCrossentropy
import numpy as np
import json
import os

app = Flask(__name__)

def load_machine_learning_model(path_model_structure, path_model_weights):
        # load json and create model
        with open(path_model_structure, "r") as json_file:
            loaded_model_json = json_file.read()
            
        loaded_model = model_from_json(loaded_model_json)
        # load weights into new model
        loaded_model.load_weights(path_model_weights)
        print("Loaded model from disk")

        loss_fn = SparseCategoricalCrossentropy(from_logits=True)
        # evaluate loaded model on test data
        loaded_model.compile(optimizer='adam',
                            loss=loss_fn,
                            metrics=['accuracy'])
        return loaded_model

@app.route('/prediction', methods=['POST', 'GET'])
def make_prediction():

    if request.method == 'POST':

        width, height = 28, 28
        image_input= request.get_json()
        image_array = np.array(image_input['data'])
        image_array = image_array.reshape(1, width, height)

        result = dict()
        result['label'] = str(loaded_model.predict_classes(image_array)[0])
        result['shape'] = f"{width}, {height}" 
        result['status'] = '200'
        
        return jsonify(result)
    else:
        return 'Riri is in the place.'


if __name__ == '__main__':

    PATH_MODEL_STRUCTURE = "../Models/model.json"
    PATH_MODEL_WEIGHTS = "../Models/model.h5"

    loaded_model = load_machine_learning_model(path_model_structure=PATH_MODEL_STRUCTURE, 
                                               path_model_weights=PATH_MODEL_WEIGHTS)

    app.run(host='0.0.0.0', 
            port=10000, 
            debug=True)