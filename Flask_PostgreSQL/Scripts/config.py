import os

user = 'tf'
password = 'toto'
host = 'localhost'
database = 'sample_db'
port = 5432

DATABASE_CONNECTION_URI = f'postgresql+psycopg2://{user}:{password}@{host}:{port}/{database}'