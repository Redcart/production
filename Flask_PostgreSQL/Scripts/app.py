from flask import Flask, request, jsonify
import tensorflow as tf
from tensorflow.keras.models import load_model
from tensorflow.keras.losses import SparseCategoricalCrossentropy
import numpy as np
import json
import os
from models import db, Prediction
from config import DATABASE_CONNECTION_URI 


app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = DATABASE_CONNECTION_URI 
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.app_context().push()
db.init_app(app)
db.create_all()

def load_machine_learning_model(path_model):
            
        loaded_model = load_model(path_model)
        print("Loaded model from disk")

        loss_fn = SparseCategoricalCrossentropy(from_logits=True)
        # evaluate loaded model on test data
        loaded_model.compile(optimizer='adam',
                            loss=loss_fn,
                            metrics=['accuracy'])
        return loaded_model

@app.route('/', methods=['GET'])
def fetch_predictions():
    predictions = Prediction.query.all()
    all_predictions = []
    for pred in predictions:
        new_pred = {
            "id": pred.id,
            "label": pred.label,
            "shape": pred.shape,
            "status": pred.status
        }

        all_predictions.append(new_pred)

    return jsonify(all_predictions), 200


@app.route('/prediction', methods=['POST'])
def make_prediction():

    if request.method == 'POST':

        width, height = 28, 28
        image_input= request.get_json()
        image_array = np.array(image_input['data'])
        image_array = image_array.reshape(1, width, height)

        predictions = loaded_model.predict(image_array) 
        class_predicted = np.argmax(predictions, axis=1)
        label = str(class_predicted)
        shape = f"{width}, {height}" 
        status = '200'

        instance = Prediction(label=label, shape=shape, status=status)
        db.session.add(instance)
        db.session.commit()

        return json.dumps('Prediction added to PostgreSQL database.')
    else:
        return 'Riri is in the place.'


if __name__ == '__main__':

    PATH_MODEL = "../Models/final_model.h5"

    loaded_model = load_machine_learning_model(path_model=PATH_MODEL)

    app.run(host='0.0.0.0', 
            port=10000, 
            debug=True)