import flask_sqlalchemy

db = flask_sqlalchemy.SQLAlchemy()


class Prediction(db.Model):
    __tablename__ = 'predictions'
    id = db.Column(db.Integer, primary_key=True)
    label = db.Column(db.String(30))
    shape = db.Column(db.String(30))
    status = db.Column(db.String(30))