from flask import Flask, request, jsonify
import tensorflow as tf
from tensorflow.keras.models import model_from_json
from tensorflow.keras.models import load_model
from tensorflow.keras.losses import SparseCategoricalCrossentropy
import numpy as np
import json
import os

app = Flask(__name__)

def load_machine_learning_model(path_model):
            
        loaded_model = load_model(path_model)
        print("Loaded model from disk")

        loss_fn = SparseCategoricalCrossentropy(from_logits=True)
        # evaluate loaded model on test data
        loaded_model.compile(optimizer='adam',
                            loss=loss_fn,
                            metrics=['accuracy'])
        return loaded_model

@app.route('/prediction', methods=['POST', 'GET'])
def make_prediction():

    if request.method == 'POST':

        width, height = 28, 28
        image_input= request.get_json()
        image_array = np.array(image_input['data'])
        image_array = image_array.reshape(1, width, height)

        result = dict()
        predictions = loaded_model.predict(image_array) 
        class_predicted = np.argmax(predictions, axis=1)
        result['label'] = str(class_predicted)
        result['shape'] = f"{width}, {height}" 
        result['status'] = '200'
        
        return jsonify(result)
    else:
        return 'Riri is in the place.'


if __name__ == '__main__':

    PATH_MODEL = "../Models/final_model.h5"

    loaded_model = load_machine_learning_model(path_model=PATH_MODEL)

    app.run(host='0.0.0.0', 
            port=10000, 
            debug=True)