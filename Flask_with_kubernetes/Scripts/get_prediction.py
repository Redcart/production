import numpy as np 
import pandas as pd
import requests
from cv2 import imread


def get_prediction_from_image(path_image, flask_url):
    
    image = imread(path_image, 0)
    image_json = pd.DataFrame(image).to_json(orient='split')

    headers = {'content-type': 'application/json', 'Accept-Charset': 'UTF-8'}
    request_result = requests.post(url=flask_url, 
                                   data=image_json, 
                                   headers=headers)
    #request_result = requests.get(flask_url, json=image_json)

    return request_result


if __name__ == '__main__':

    PATH_IMAGE = '../Data/sample.png'
    FLASK_URL = 'http://127.0.0.1:80/prediction'


    r = get_prediction_from_image(path_image=PATH_IMAGE, 
                                  flask_url=FLASK_URL)
    print(r.text)
    print(r.status_code)