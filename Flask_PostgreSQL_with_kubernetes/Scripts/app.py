from flask import Flask, request, jsonify
import tensorflow as tf
from tensorflow.keras.models import model_from_json
from tensorflow.keras.losses import SparseCategoricalCrossentropy
import numpy as np
from models import db, Prediction
import json
import os

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = os.environ['SQLALCHEMY_DATABASE_URI']
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.app_context().push()
db.init_app(app)
db.create_all()

def load_machine_learning_model(path_model_structure, path_model_weights):
        # load json and create model
        with open(path_model_structure, "r") as json_file:
            loaded_model_json = json_file.read()
            
        loaded_model = model_from_json(loaded_model_json)
        # load weights into new model
        loaded_model.load_weights(path_model_weights)
        print("Loaded model from disk")

        loss_fn = SparseCategoricalCrossentropy(from_logits=True)
        # evaluate loaded model on test data
        loaded_model.compile(optimizer='adam',
                            loss=loss_fn,
                            metrics=['accuracy'])
        return loaded_model

@app.route('/', methods=['GET'])
def fetch():
    predictions = Prediction.query.all()
    all_predictions = []
    for pred in predictions:
        new_pred = {
            "id": pred.id,
            "label": pred.label,
            "shape": pred.shape,
            "status": pred.status
        }

        all_predictions.append(new_pred)

    return jsonify(all_predictions), 200


@app.route('/prediction', methods=['POST'])
def hello_world():

    if request.method == 'POST':

        width, height = 28, 28
        image_input= request.get_json()
        image_array = np.array(image_input['data'])
        image_array = image_array.reshape(1, width, height)

        label = str(loaded_model.predict_classes(image_array)[0])
        shape = f"{width}, {height}" 
        status = '200'

        instance = Prediction(label=label, shape=shape, status=status)
        db.session.add(instance)
        db.session.commit()

        return json.dumps('Prediction added to Postgre SQL database.')
    else:
        return 'Riri is in the place.'


if __name__ == '__main__':

    PATH_MODEL_STRUCTURE = "../Models/model.json"
    PATH_MODEL_WEIGHTS = "../Models/model.h5"

    loaded_model = load_machine_learning_model(path_model_structure=PATH_MODEL_STRUCTURE, 
                                               path_model_weights=PATH_MODEL_WEIGHTS)

    app.run(host='0.0.0.0', 
            port=10000, 
            debug=True)